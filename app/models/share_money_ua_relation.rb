class ShareMoneyUaRelation < ActiveRecord::Base
	validates :user_id, :account_id, presence: true
	belongs_to :user, :class_name => "ShareMoneyUser", :foreign_key => "user_id"
	belongs_to :account, :class_name => "ShareMoneyAccount", :foreign_key => "account_id"
	has_many :flows, :class_name => "ShareMoneyFlow", :foreign_key => "relation_id", :dependent => :destroy
end
