class ShareMoneyItem < ActiveRecord::Base
	validates :name, :price, :timestamp, :account_id, presence: true
	belongs_to :account, :class_name => "ShareMoneyAccount", :foreign_key => "account_id"
	has_many :flows, :class_name => "ShareMoneyFlow", :foreign_key => "item_id", :dependent => :destroy
	belongs_to :tag, :class_name => "ShareMoneyTag", :foreign_key => "tag_id"
	belongs_to :currency, :class_name => "ShareMoneyCurrency", :foreign_key => "currency_id"
end
