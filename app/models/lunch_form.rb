class LunchForm < ActiveRecord::Base
	has_many :lunch_order
	has_many :lunch_commodity, :through => :lunch_order
end
