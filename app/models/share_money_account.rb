class ShareMoneyAccount < ActiveRecord::Base
	validates :owner_id, :name, presence: true
	belongs_to :owner, :class_name => "ShareMoneyUser", :foreign_key => "owner_id"
	has_many :uarelations, :class_name => "ShareMoneyUaRelation", :foreign_key => "account_id", :dependent => :destroy
	has_many :users, :through => :uarelations, :source => :user
	has_many :items, :class_name => "ShareMoneyItem", :foreign_key => "account_id", :dependent => :destroy
	has_many :flows, :through => :items, :source => :flows
end
