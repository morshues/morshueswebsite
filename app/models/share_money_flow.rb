class ShareMoneyFlow < ActiveRecord::Base
	validates :item_id, presence: true
	belongs_to :item, :class_name => "ShareMoneyItem", :foreign_key => "item_id"
	belongs_to :relation, :class_name => "ShareMoneyUaRelation", :foreign_key => "relation_id"
end
