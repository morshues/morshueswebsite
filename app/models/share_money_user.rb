class ShareMoneyUser < ActiveRecord::Base
	has_secure_password
	validates :name, presence: true
	validates :password, length: { minimum: 6 }
	has_many :uarelations, :class_name => "ShareMoneyUaRelation", :foreign_key => "user_id", :dependent => :destroy
	has_many :accounts, :through => :uarelations, :source => :account
	has_many :flows, :through => :uarelations, :source => :flow
end
