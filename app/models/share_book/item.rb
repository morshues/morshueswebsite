class ShareBook::Item < ApplicationRecord
  belongs_to :book, :class_name => 'ShareBook::Book', :foreign_key => 'book_id'
  has_many :flows, :class_name => 'ShareBook::Flow', :foreign_key => 'item_id', :dependent => :destroy
  belongs_to :exchange, :class_name => 'ShareBook::Exchange', :foreign_key => 'exchange_id'

  accepts_nested_attributes_for :flows, :allow_destroy => true

  before_save :init_price

  def init_price
    self.price = 0 if self.price.nil?
    self.flows = self.flows.reject{|flow| flow.number == 0}
  end

  def time_str
    self.time.strftime("%m/%d %H:%M")
  end

  def member_flow member
    self.flows.find_by(:member_id => member.id)
  end

  def member_flow_num member
    member_flow(member)&.number || 0
  end

  def exchange_name
    self.exchange.currency
  end

end
