class ShareBook::Flow < ApplicationRecord
  belongs_to :member, :class_name => "ShareBook::Member", :foreign_key => "member_id"
  belongs_to :flow, :class_name => "ShareBook::Flow", :foreign_key => "flow_id"

  before_save :init_num

  def init_num
    self.number = 0 if self.number.nil?
  end

end
