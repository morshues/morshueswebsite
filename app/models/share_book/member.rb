class ShareBook::Member < ApplicationRecord
  belongs_to :book, :class_name => "ShareBook::Book", :foreign_key => "book_id"
  has_many :flows, :class_name => "ShareBook::Flow", :foreign_key => "member_id", :dependent => :destroy

  after_create :gen_key

  def gen_key
    loop do
      key = SecureRandom.hex(5)
      unless ShareBook::Member.exists?(key: key)
        self.key = key
        self.save
        return
      end
    end
  end

  def self.validate token, key
    member = self.where(key: key).first
    return member if member.present? && member.book.token == token
    return nil
  end

  def sum_price
    self.flows.sum(&:number)
  end

end
