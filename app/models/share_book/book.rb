class ShareBook::Book < ApplicationRecord
  has_many :members, -> { order(id: :asc) }, :class_name => "ShareBook::Member", :foreign_key => "book_id", :dependent => :destroy
  has_many :items, -> { order(time: :desc, id: :desc) }, :class_name => "ShareBook::Item", :foreign_key => "book_id", :dependent => :destroy
  has_many :exchanges, :class_name => "ShareBook::Exchange", :foreign_key => "book_id", :dependent => :destroy

  after_create :gen_token

  def gen_token
    loop do
      token = SecureRandom.hex(5)
      unless ShareBook::Book.exists?(token: token)
        self.token = token
        self.save
        return
      end
    end
  end

  def sum_price
    self.items.sum(:price)
  end

  def balance
    self.members.sum(&:sum_price)-self.sum_price
  end

end
