class LunchCommodity < ActiveRecord::Base
	belongs_to :lunch_store
	has_many :lunch_order
	has_many :lunch_form, :through => :lunch_order
end
