require 'net/http'
require 'json'

class MWeatherController < ApplicationController
	WEATHER_URL = "http://opendata.cwb.gov.tw/opendata/MFC/F-D0047-%s.xml"
	DATA_INDEX_TAIPEI_TWODAY = '061'
	DATA_INDEX_TAIPEI_WEEK = '063'
	DATA_INDEX_MIAOLI_TWODAY = '013'
	DATA_INDEX_MIAOLI_WEEK = '015'
	DATA_INDEX_ZHONGSHAN = 3	# 中山區
	DATA_INDEX_JHONGJHENG = 4	# 中正區
	DATA_INDEX_WANHUA = 9		# 萬華區
	DATA_INDEX_SONGSHAN = 10	# 松山區
	DATA_INDEX_SHINYI = 11		# 信義區
	DATA_INDEX_JUNAN = 12		# 竹南鎮

	def index
		data = getWeatherSource(WEATHER_URL%[DATA_INDEX_TAIPEI_TWODAY])

		$city_name = data[DATA_INDEX_ZHONGSHAN]['locationName']

		zhongshanData = data[DATA_INDEX_ZHONGSHAN]['weatherElement']
		jhongjhengData = data[DATA_INDEX_JHONGJHENG]['weatherElement']
		wanhuaData = data[DATA_INDEX_WANHUA]['weatherElement']
		songshanData = data[DATA_INDEX_SONGSHAN]['weatherElement']
		shinyiData = data[DATA_INDEX_SHINYI]['weatherElement']

		$visualData = formatTwoDay(zhongshanData)
		# render json: $visualData


		
	end

	def formatTwoDay(data)
		result = {}

		# TimeTag
		name = "time"
		result[name] = {}
		values = data[0]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timestamp.strftime("%H:%M")
		}

		# DateTag
		name = "date"
		result[name] = {}
		values = data[0]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timestamp.strftime("%m/%d")
		}


		# T
		name = data[0]["elementName"]
		result[name] = {}
		values = data[0]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# Td
		name = data[1]["elementName"]
		result[name] = {}
		values = data[1]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# RH
		name = data[2]["elementName"]
		result[name] = {}
		values = data[2]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# # Wind
		# name = data[3]["elementName"]
		# result[name] = {}
		# values = data[3]["time"]
		# values.each { |timeData|
		# 	timestamp = Time.parse(timeData["dataTime"])
		# 	result[name][timestamp] = timeData["parameter"]
		# }

		# AT
		name = data[4]["elementName"]
		result[name] = {}
		values = data[4]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# CI
		name = data[5]["elementName"]
		result[name] = {}
		values = data[5]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["dataTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# Wx
		name = data[6]["elementName"]
		result[name] = {}
		values = data[6]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["parameter"]["parameterValue"]
		}

		# PoP
		name = data[7]["elementName"]
		result[name] = {}
		values = data[7]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# PoP6h
		name = data[8]["elementName"]
		result[name] = {}
		values = data[8]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# # WeatherDescriptinameon
		# name = data[8]["elementName"]
		# result[name] = {}
		# values = data[8]["time"]
		# values.each { |timeData|
		# 	timestamp = Time.parse(timeData["startTime"])
		# 	result[name][timestamp] = timeData["elementValue"]["value"]
		# }

		return result
	end

	def formatWeek(data)
		result = {}

		# T
		name = data[0]["elementName"]
		result[name] = {}
		values = data[0]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# Td
		name = data[1]["elementName"]
		result[name] = {}
		values = data[1]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# RH
		name = data[2]["elementName"]
		result[name] = {}
		values = data[2]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# # Wind
		# name = data[3]["elementName"]
		# result[name] = {}
		# values = data[3]["time"]
		# values.each { |timeData|
		# 	timestamp = Time.parse(timeData["startTime"])
		# 	result[name][timestamp] = timeData["parameter"]
		# }

		# Wx
		name = data[4]["elementName"]
		result[name] = {}
		values = data[4]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# PoP
		name = data[5]["elementName"]
		result[name] = {}
		values = data[5]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# MaxT
		name = data[6]["elementName"]
		result[name] = {}
		values = data[6]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# MinT
		name = data[7]["elementName"]
		result[name] = {}
		values = data[7]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# MaxCI
		name = data[8]["elementName"]
		result[name] = {}
		values = data[8]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# MinCI
		name = data[9]["elementName"]
		result[name] = {}
		values = data[9]["time"]
		values.each { |timeData|
			timestamp = Time.parse(timeData["startTime"])
			result[name][timestamp] = timeData["elementValue"]["value"]
		}

		# # WeatherDescription
		# name = data[10]["elementName"]
		# result[name] = {}
		# values = data[10]["time"]
		# values.each { |timeData|
		# 	timestamp = Time.parse(timeData["startTime"])
		# 	result[name][timestamp] = timeData["elementValue"]["value"]
		# }

		return result
	end

  	def getXML(urlPath, oldObj)
		url = URI.parse(urlPath)
		req = Net::HTTP::Get.new(url.to_s)
		res = Net::HTTP.start(url.host, url.port) {|http|
		  http.request(req)
		}
		begin
			if oldObj
				wtmp = oldObj
				wtmp.update(:data => Hash.from_xml(res.body))	
				data = wtmp.data
			else
				wtmp = Weathertmp.new
				wtmp.key = urlPath
				wtmp.data = Hash.from_xml(res.body)	
				data = wtmp.data
				wtmp.save	# save after trans data to Hash to avoid exception
			end
			return data
		rescue
			return {GetXMLFailure:res}
		end
	end

	def getWeatherSource(urlPath)
		result = Weathertmp.where(:key=>urlPath).first
		if result == nil || (result[:updated_at] + 3600 < Time.now)
			result = getXML(urlPath, result)
		else
			result = result.data
		end
		result = removeLineBreak(result['cwbopendata']['dataset']['locations']['location'])
		return result
	end

	def removeLineBreak(data)
		if data.class == Hash
			data.map { |key, item|
				removeLineBreak(item)
			}
		elsif data.class == Array
			data.each { |item|
				removeLineBreak(item)
			}
		elsif data.class == String
			data.gsub!(/\n +/, "")
		end
	end

end
