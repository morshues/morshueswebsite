class ShareMoneyAccountController < ApplicationController
	protect_from_forgery
	def index
		if not session[:id]
			redirect_to :action => :login
			return
		end

		if ShareMoneyUser.exists?(session[:id])
			@user = ShareMoneyUser.find(session[:id].to_i)
			@has_account = true
		else
			@has_account = false
		end

		# render json: @owner.accounts
	end

	def signup
		if ShareMoneyUser.exists?(session[:id])
			redirect_to :action => :index
		else
			session.delete :id
			@user = ShareMoneyUser.new
		end
	end
	def createuser
		if ShareMoneyUser.exists?(:name => user_params['name'])
			flash[:msg_share_money_signup] = '帳號已被使用'
			redirect_to :action => :signup
			return
		end
		if user_params[:password] != user_params[:password_confirmation]
			flash[:msg_share_money_signup] = '兩次輸入密碼不相同'
			redirect_to :action => :signup
			return
		end
		
		@user = ShareMoneyUser.new(user_params)
		if @user.save
			session[:id] = @user.id
			redirect_to :action => :index
		else
			flash[:msg_share_money_signup] = '帳號建立失敗'
			redirect_to :action => :signup
		end

	end

	def login
		if session[:id]
			redirect_to :action => :index
		end
	end
	def dologin
		if params[:session] and params[:session][:name] and params[:session][:password]
			@user = ShareMoneyUser.find_by(name: params[:session][:name])  
			if @user && @user.authenticate(params[:session][:password])  
				session[:id] = @user.id
				redirect_to :action => :index
			else
				render plain: '無此帳號'				
			end
		else
			redirect_to :action => :login
		end
	end
	def logout
		session.delete :id
		redirect_to :action => :login
	end

	def account
		if !/\A[\d]+\z/.match(params[:rid])
			redirect_to :action => :index
		end

		rel_id = params[:rid].to_i
		if not ShareMoneyUaRelation.exists?(rel_id)
			redirect_to :action => :index
		end
		@relation = ShareMoneyUaRelation.find(rel_id)

		if @relation.user.id != session[:id]
			redirect_to :action => :index
		end

		@item = ShareMoneyItem.new
		@item.account_id = @relation.account_id
	end

	def newaccount
		if not ShareMoneyUser.exists?(session[:id])
			redirect_to :action => :index
			return
		end

		@account = ShareMoneyAccount.new
	end
	def createaccount
		owner_id = session[:id]
		@account = ShareMoneyAccount.new(account_params)
		@account.owner_id = owner_id
		@account.save

		ua_rel =  ShareMoneyUaRelation.new
		ua_rel.user_id = owner_id
		ua_rel.account_id = @account.id
		ua_rel.save

		redirect_to :action => :account, :rid => ua_rel.id
	end

	def createrel
		new_user = ShareMoneyUser.find_by(:name => params[:user_name])
		ori_rel = ShareMoneyUaRelation.find(params[:rid])
		not_duplicate = ShareMoneyUaRelation.find_by(:user_id => new_user, :account_id => ori_rel.account.id) == nil
		if new_user and not_duplicate
			params[:share_money_ua_relation][:user_id] = new_user.id
			@rel = ShareMoneyUaRelation.new(rel_params)
			if ShareMoneyUser.exists?(@rel.user_id) and ShareMoneyAccount.exists?(@rel.account_id)
				# render plain: @rel.account.uarelations.exists?(@rel.user)
				# return
				@rel.save
			end
		end
		redirect_to :action => :account, :rid => params[:rid]
	end

	def createitem		
		relation = ShareMoneyUaRelation.find(params[:rid])
		params[:share_money_item][:account_id] = relation.account_id
		@item = ShareMoneyItem.new(item_params)
		@item.save

		params[:share_money_flow] = {}
		params[:share_money_flow][:item_id] = @item.id

		if @item.mode == 'income' or @item.mode == 'complex' or @item.mode == 'advance'
			case params['founder']
			when 'single'
				params[:share_money_flow][:num] = @item.price
				params[:share_money_flow][:relation_id] = params['flow_founder']
				flow = ShareMoneyFlow.new(flow_params)
				flow.save
			when 'average'
				average_price = @item.price / relation.account.users.size
				params[:share_money_flow][:num] = average_price
				relation.account.users.each do |user|
					params[:share_money_flow][:relation_id] = ShareMoneyUaRelation.find_by(:user_id => user.id, :account_id => relation.account_id).id
					flow = ShareMoneyFlow.new(flow_params)
					flow.save
				end
			when 'advance'
				# todo
			end
		end

		if @item.mode == 'outlay' or @item.mode == 'complex' or @item.mode == 'advance'
			case params['spender']
			when 'single'
				params[:share_money_flow][:num] = -@item.price
				params[:share_money_flow][:relation_id] = params['flow_spender']
				flow = ShareMoneyFlow.new(flow_params)
				flow.save
			when 'average'
				average_price = @item.price / relation.account.users.size
				params[:share_money_flow][:num] = -average_price
				relation.account.users.each do |user|
					params[:share_money_flow][:relation_id] = ShareMoneyUaRelation.find_by(:user_id => user.id, :account_id => relation.account_id).id
					flow = ShareMoneyFlow.new(flow_params)
					flow.save
				end
			when 'advance'
				# todo
			end
		end

		redirect_to :action => :account, :rid => params[:rid]
	end
	def destroyitem
		@item = ShareMoneyItem.find(params[:id])
		@item.destroy

		redirect_to :action => :account, :rid => params[:rid]
	end
	def user_params
		params.require(:share_money_user).permit(:name, :password, :password_confirmation)
	end
	def account_params
		params.require(:share_money_account).permit(:name)
	end
	def rel_params
		params.require(:share_money_ua_relation).permit(:user_id, :account_id)
	end
	def item_params
		params.require(:share_money_item).permit(:name, :price, :account_id, :timestamp, :mode, :tag_id, :currency_id)
	end
	def flow_params
		params.require(:share_money_flow).permit(:item_id, :relation_id, :num)
	end
end
