class UrlController < ApplicationController
	$short_url_root_path = 'http://u.morshues.com/'

  def index
  	@short_url = nil
    result = Shortener::ShortenedUrl.generate(params[:origin_url])

    if result and result['unique_key']
		@short_url = $short_url_root_path + result['unique_key']
	end

	@history = Shortener::ShortenedUrl.all.order(created_at: :desc)
  end

  def temp_redirect
    shortener = Shortener::ShortenedUrl.find_by(unique_key: params[:id])
    shortener.update_column(:use_count, shortener.use_count+1)
    redirect_to shortener.url, prarms: params, status: :moved_permanently
  end

end