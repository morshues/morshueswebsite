class LunchController < ApplicationController
	def index
		@lunch_forms = LunchForm.where(:status => 0)

	end

	def list
		@stores = LunchStore.all

	end

	def show
		@message = params[:server_message]
		form_id = params[:id].to_i

		if LunchForm.exists?(form_id)
			@form_id = form_id
			@form_data = LunchForm.find(form_id)
			@store_data = LunchStore.find(@form_data.store_id)
			@commodity_data = LunchCommodity.where(:store_id => @store_data.id)

		else
			render plain: '無此訂單'
		end
	end

	def updateform
		form_id = params[:id].to_i
		order_name = params[:text_order_name]
		if not order_name or order_name == ''
			redirect_to action: 'show', id: form_id, server_message: '請輸入訂購人姓名'
			return
		end
		new_order_data = params[:text_order_value]
		new_order_data.each do |order_key, order_value|
			order_count = order_value.to_i
			if order_count != 0
				order = LunchOrder.new(:form_id => form_id, :commodity_id => order_key, :count => order_value.to_i, :owner => order_name)
				order.save
			end
		end
		redirect_to action: 'form', id: form_id
	end

	def form
		# render plain: params
		# return

		form_id = params[:id].to_i
		@form_data = LunchForm.find(form_id)
		@store_data = LunchStore.find(@form_data.store_id)
		@order_data = LunchOrder.where(:form_id => form_id).order(:created_at)

		@order_summary = {}
		@order_data_groupby_person = {}
		@order_data.each do |order_item|
			commodity_data = LunchCommodity.find(order_item[:commodity_id])
			commodity_name = commodity_data[:name]
			if @order_summary.has_key?(commodity_name)
				@order_summary[commodity_name][:count] += order_item[:count]
			else
				@order_summary[commodity_name] = {:count => order_item[:count], 
					:price => commodity_data[:price]}
			end

			owner = order_item[:owner]
			if @order_data_groupby_person.has_key?(owner)
				if @order_data_groupby_person[owner][:order].has_key?(commodity_name)
					@order_data_groupby_person[owner][:order][commodity_name] += order_item[:count]
				else
					@order_data_groupby_person[owner][:order][commodity_name] = order_item[:count]
				end
				@order_data_groupby_person[owner][:sum_price] += commodity_data[:price]*order_item[:count]
			else
				@order_data_groupby_person[owner] = {:order => {commodity_name => order_item[:count]}, 
					:sum_price => commodity_data[:price]*order_item[:count]}
			end
		end
		@total_price = 0
		@order_summary.each do |order_key, order_item|
			@order_summary[order_key][:price_sum] = @order_summary[order_key][:count] * @order_summary[order_key][:price]
			@total_price += @order_summary[order_key][:price_sum]
		end

		# render plain: @order_data_groupby_person
		# form_id = params[:id].to_i

		# if LunchForm.exists?(form_id)
		# end

	end

	def newform
		store_name = params[:name]
		store_data = LunchStore.where(:name => store_name)

		if store_data.length == 0
			redirect_to action: 'newstore', name: store_name
		elsif store_data.length == 1
			form = LunchForm.new(:store_id => store_data[0].id, :status => 0, :sum_price => 0)
			form.save
			redirect_to action: 'form', id: form.id
		else
			render plain: 'store error'
			return
		end				
	end

	def newstore
		store_name = params[:name]
		store_data = LunchStore.where(:name => store_name)

		is_new_store = (store_data.length == 0)
		if is_new_store
			@store_name = '新餐廳'
		else
			@store_name = store_data[0][:name]
			@store_phone = store_data[0][:phone]
			store_id = store_data[0][:id]
			@commodity_data = LunchCommodity.where(:store_id => store_id)
		end


		# render plain: store_data
	end

	def updatecommodity
		store_name = params[:text_store_name]
		if params[:commit] == '更新餐廳'
			store_data = LunchStore.where(:name => store_name)
			store_phone = params[:text_store_phone]
			if store_data.length == 0
				store_item = LunchStore.new(:name => store_name, :phone => store_phone, :count => 0)
				store_item.save
			elsif store_data.length == 1
				store_item = store_data[0]
				store_item.update(:name => store_name, :phone => store_phone)
			else
				render plain: 'store error'
				return
			end
		elsif params[:commit] == '增加項目'
			store_data = LunchStore.where(:name => store_name)
			if store_data.length == 0
				store_item = LunchStore.new(:name => store_name, :count => 0)
				store_item.save

				store_id = store_item[:id]
			elsif store_data.length == 1
				store_id = store_data[0][:id]
			else
				render plain: 'store error'
				return
			end
			item_name = params[:text_commodity_name]
			item_price = params[:text_commodity_price]
			if item_name != '' && item_price != ''
				commodity_item = LunchCommodity.create(:store_id => store_id, :name => item_name, :price => item_price.to_i, :count => 0)
				commodity_item.save
			end
		else
			deleteitem = params['deleteitem']
			if deleteitem
				commodity_id = deleteitem.keys[0]
				LunchCommodity.find(commodity_id).delete
			else
				render plain: 'item error'
			end
		end

		redirect_to action: 'newstore', name: store_name
	end

	def manage
		if params[:magicnumber] != 'morshues'
			redirect_to action: 'index'
			return
		end

		@lunch_forms = LunchForm.all

		@lunch_stores = LunchStore.all

	end

	def remove
		if params[:magicnumber] != 'morshues'
			redirect_to action: 'index'
			return
		end

		form_id = params[:id].to_i
		form_data = LunchForm.find(form_id)
		order_data = LunchOrder.where(:form_id => form_id)
		order_data.each do |order_item|
			order_item.delete
		end
		form_data.delete

		redirect_to action: 'index'
	end

	def remove_store
		if params[:magicnumber] != 'morshues'
			redirect_to action: 'index'
			return
		end

		store_id = params[:id]
		store_data = LunchStore.find(store_id)
		lunch_commodities = LunchCommodity.where(:store_id => store_id)
		lunch_forms = LunchForm.where(:store_id => store_id)
		lunch_forms.each do |form_data|
			order_data = LunchOrder.where(:form_id => form_data.id)
			order_data.each do |order_item|
				order_item.delete
			end
			form_data.delete
		end

		lunch_commodities.each do |commodity_item|
			order_data = LunchOrder.where(:commodity_id => commodity_item.id)
			order_data.each do |order_item|
				order_item.delete
			end
			commodity_item.delete
		end
		store_data.delete

		redirect_to action: 'index'
	end

end
