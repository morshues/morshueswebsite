class ShareBookController < ApplicationController

  def set_member token, key
    session[:token] = token
    session[:key] = key
    session[:o_token] = token
    session[:o_key] = key
    member = current_member
    unless member.present?
      clear_member
    end
    return member
  end

  def current_member
    return ShareBook::Member.validate(session[:token], session[:key])
  end

  def old_member
    return ShareBook::Member.validate(session[:o_token], session[:o_key])
  end

  def clear_member
    session[:token] = nil
    session[:key] = nil
  end

  def reset_history
    session[:o_token] = nil
    session[:o_key] = nil
  end

  def has_permission?
    current_member.present?
  end

end
