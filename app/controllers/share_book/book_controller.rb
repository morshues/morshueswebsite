class ShareBook::BookController < ShareBookController
  before_action { redirect_to share_book_path unless has_permission? }
  before_action :setup

  def show
  end

  def update
    @book.update(book_params)
  end

  def create_item
    params[:share_book_item][:price] = params[:share_book_item][:price].to_i * [-1, 1][params[:share_book_item][:sign].to_i]
    params[:share_book_item][:flows_attributes].each do |index, flow|
      flow[:number] = flow[:number].to_i * [-1, 1][flow[:sign].to_i]
    end
    # render json: params
    # return
    @book.items.create(item_params)

    redirect_to share_book_book_path
  end

  def members
  end

  def create_member
    @book.members.create(member_params)

    redirect_to members_share_book_book_path
  end

  def update_member
    @current_member.update(member_params)
    respond_to do |format|
      format.js
    end
  end

  def delete_item
    item = @book.items.find(params[:item_id])
    item.destroy

    redirect_to share_book_book_path
  end

  def create_exchange
    @book.exchanges.create(exchange_params)

    redirect_to members_share_book_book_path
  end

  def update_exchange
    exchange = @book.exchanges.find(params[:edit_id])
    exchange.update(exchange_params)
    respond_to do |format|
      format.js
    end
  end
  
  def logout
    clear_member
    redirect_to share_book_path
  end

private
  def setup
    @current_member = current_member
    @book = @current_member.book
  end

  def book_params
    params.require(:share_book_book).permit(:name)
  end

  def member_params
    params.require(:share_book_member).permit(:nick_name)
  end

  def item_params
    params.require(:share_book_item).permit(:title, :price, :time, flows_attributes: [:member_id, :number])
  end

  def exchange_params
    params.require(:share_book_exchange).permit(:currency, :value)
  end

end
