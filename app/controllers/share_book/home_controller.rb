class ShareBook::HomeController < ShareBookController
  layout "share_book"

  def index
    @old_member = old_member
  end

  def login
    member = set_member params[:token], params[:key]
    if member.present?
      redirect_to share_book_book_path(member)
    else
      @msg = 'Book not found'
      respond_to do |format|
        format.js
      end
    end
  end

  def reset
    reset_history
    redirect_to share_book_path
  end

  def create_book
    book = ShareBook::Book.create(
      name: 'new book',
      currency_name: 'TWD',
    )
    member = book.members.create(
      nick_name: 'new member',
    )

    set_member book.token, member.key

    redirect_to share_book_book_path(member.id)
  end

end
