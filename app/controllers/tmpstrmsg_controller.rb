class TmpstrmsgController < ApplicationController
	def index
		objs = Tmpstrmsg.all
		render json: objs.to_json
	end
	skip_before_action :verify_authenticity_token
	def add
		msg_obj = Tmpstrmsg.new
		msg_obj['title'] = params['title'] if params['title'] != nil
		msg_obj['msg'] = params['msg'] if params['msg'] != nil
		msg_obj['more_msg'] = params['more_msg'] if params['more_msg'] != nil
		msg_obj.save
		render plain: "success"
	end
end
