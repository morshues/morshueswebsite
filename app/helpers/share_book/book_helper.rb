module ShareBook::BookHelper
  def tri_selector arr, num
    return arr[num <=> 0]
  end

  def bound_str num, reverse = false
    return tri_selector ['', 'out-bound', 'in-bound'], num*(reverse ? 1:-1)
  end
end
