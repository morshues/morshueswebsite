// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('turbolinks:load', function(event) {
  var menu_icon = $('.menu-icon');
  if (menu_icon.length > 0) {
    menu_icon.click(function(e) {
      menu_icon.find('div').toggleClass("change");
      var menu = $('.menu')
      menu.fadeToggle(200);
    });
  }

  var create_popup = $('#btn-create-popup');
  if (create_popup.length > 0) {
    create_popup.click(function(e) {
      $('.create-block').fadeIn(200);
    });
  }

  var create_close = $('#btn-create-close');
  if (create_close.length > 0) {
    create_close.click(function(e) {
      $('.create-block').fadeOut(200);
    });
  }

});