$(document).on('turbolinks:load', function(event) {
  var create_block = $('.create-block');
  if (create_block.length > 0) {
    $('.create-block .create-content .item .hint').click(function(e) {
      var d = new Date();
      $(".item-date#share_book_item_time_1i").val(d.getYear()+1900);
      $(".item-date#share_book_item_time_2i").val(d.getMonth()+1);
      $(".item-date#share_book_item_time_3i").val(d.getDate());
      $(".item-date#share_book_item_time_4i").val(("0" + d.getHours()).slice(-2));
      $(".item-date#share_book_item_time_5i").val(("0" + d.getMinutes()).slice(-2));
    });
  }

});