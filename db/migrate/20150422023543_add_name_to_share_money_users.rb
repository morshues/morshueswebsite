class AddNameToShareMoneyUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_users, :name, :string
  end
end
