class CreateShareMoneyUaRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_ua_relations do |t|
      t.integer :account_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
