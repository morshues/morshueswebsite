class CreateShareMoneyTags < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_tags do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
