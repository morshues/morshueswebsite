class CreateShareMoneyAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_accounts do |t|

      t.timestamps null: false
    end
  end
end
