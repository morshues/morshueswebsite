class AddNameToShareMoneyAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_accounts, :name, :string
  end
end
