class AddExpiresAtToShortenedUrl < ActiveRecord::Migration[5.1]
  def change
    add_column :shortened_urls, :expires_at, :datetime
  end
end
