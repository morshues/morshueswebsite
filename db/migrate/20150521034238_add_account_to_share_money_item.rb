class AddAccountToShareMoneyItem < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_items, :account_id, :integer
    ShareMoneyItem.all.each do |item|
    	rel = ShareMoneyUaRelation.find(item.a_u_relation_id)
    	item.update_column(:account_id, rel.account_id)
    end
    remove_column :share_money_items, :a_u_relation_id
    add_column :share_money_items, :currency_id, :integer
    add_column :share_money_items, :tag_id, :integer  	
  end
end
