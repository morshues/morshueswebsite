class CreateWeathertmps < ActiveRecord::Migration[5.1]
  def change
    create_table :weathertmps do |t|
      t.string :data
      t.date :timestamp

      t.timestamps null: false
    end
  end
end
