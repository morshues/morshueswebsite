class CreateShareBookMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :share_book_members do |t|
      t.integer :book_id
      t.integer :user_id
      t.string :nick_name
      t.string :key

      t.timestamps
    end
  end
end
