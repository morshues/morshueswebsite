class CreateShareMoneyManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_managers do |t|
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
