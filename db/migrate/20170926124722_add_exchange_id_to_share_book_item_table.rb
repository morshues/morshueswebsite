class AddExchangeIdToShareBookItemTable < ActiveRecord::Migration[5.1]
  def change
    add_column :share_book_items, :exchange_id, :float
    change_column :share_book_items, :price, :float
    change_column :share_book_flows, :number, :float
  end
end
