class AddPasswordDigestToShareMoneyUser < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_users, :password_digest, :string
  end
end
