class CreateShareBookItems < ActiveRecord::Migration[5.1]
  def change
    create_table :share_book_items do |t|
      t.integer :book_id
      t.string :title
      t.integer :price, default: 0, null: false
      t.datetime :time

      t.timestamps
    end
  end
end
