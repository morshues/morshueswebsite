class CreateShareBookBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :share_book_books do |t|
      t.integer :owner_id
      t.string :name
      t.string :currency_name
      t.string :token

      t.timestamps
    end
  end
end
