class AddModeToShareMoneyItem < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_items, :mode, :integer
  end
end
