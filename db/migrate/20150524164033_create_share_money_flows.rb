class CreateShareMoneyFlows < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_flows do |t|
      t.integer :item_id
      t.integer :relation_id
      t.float :num

      t.timestamps null: false
    end
  end
end
