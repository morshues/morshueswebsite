class CreateShareMoneyCurrencies < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_currencies do |t|
      t.string :country

      t.timestamps null: false
    end
  end
end
