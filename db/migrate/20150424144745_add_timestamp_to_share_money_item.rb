class AddTimestampToShareMoneyItem < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_items, :timestamp, :datetime
  end
end
