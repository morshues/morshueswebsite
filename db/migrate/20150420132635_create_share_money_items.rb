class CreateShareMoneyItems < ActiveRecord::Migration[5.1]
  def change
    create_table :share_money_items do |t|
      t.integer :a_u_relation_id
      t.string :name
      t.float :price

      t.timestamps null: false
    end
  end
end
