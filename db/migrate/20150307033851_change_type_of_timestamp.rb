class ChangeTypeOfTimestamp < ActiveRecord::Migration[5.1]
  def change
  	remove_column :weathertmps, :timestamp
  	remove_column :weathertmps, :data
	add_column :weathertmps, :timestamp, :time
	add_column :weathertmps, :data, :text
  end
end
