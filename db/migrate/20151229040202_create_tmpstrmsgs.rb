class CreateTmpstrmsgs < ActiveRecord::Migration[5.1]
  def change
    create_table :tmpstrmsgs do |t|
      t.string :title
      t.string :msg
      t.string :more_msg

      t.timestamps null: false
    end
  end
end
