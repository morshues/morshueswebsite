class RenameShareMoneyAccountColumnOwnerToOwnerId < ActiveRecord::Migration[5.1]
  def change
  	rename_column :share_money_accounts, :owner, :owner_id
  end
end
