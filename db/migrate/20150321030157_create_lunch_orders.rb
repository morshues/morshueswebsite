class CreateLunchOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :lunch_orders do |t|
      t.integer :form_id
      t.integer :commodity_id
      t.integer :count
      t.string :owner

      t.timestamps null: false
    end
  end
end
