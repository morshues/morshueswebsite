class CreateLunchCommodities < ActiveRecord::Migration[5.1]
  def change
    create_table :lunch_commodities do |t|
      t.integer :store_id
      t.string :name
      t.integer :price
      t.integer :count

      t.timestamps null: false
    end
  end
end
