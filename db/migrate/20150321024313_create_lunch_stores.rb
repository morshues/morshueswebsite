class CreateLunchStores < ActiveRecord::Migration[5.1]
  def change
    create_table :lunch_stores do |t|
      t.string :name
      t.string :phone
      t.integer :count
      t.datetime :last_order

      t.timestamps null: false
    end
  end
end
