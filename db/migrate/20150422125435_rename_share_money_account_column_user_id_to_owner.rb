class RenameShareMoneyAccountColumnUserIdToOwner < ActiveRecord::Migration[5.1]
  def change
  	rename_column :share_money_accounts, :user_id, :owner
  end
end
