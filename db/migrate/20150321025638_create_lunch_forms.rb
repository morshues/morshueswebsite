class CreateLunchForms < ActiveRecord::Migration[5.1]
  def change
    create_table :lunch_forms do |t|
      t.integer :store_id
      t.integer :status
      t.integer :sum_price

      t.timestamps null: false
    end
  end
end
