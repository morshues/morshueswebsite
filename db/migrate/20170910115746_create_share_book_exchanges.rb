class CreateShareBookExchanges < ActiveRecord::Migration[5.1]
  def change
    create_table :share_book_exchanges do |t|
      t.integer :book_id
      t.string :currency
      t.float :value

      t.timestamps
    end
  end
end
