class CreateShareBookFlows < ActiveRecord::Migration[5.1]
  def change
    create_table :share_book_flows do |t|
      t.integer :item_id
      t.integer :member_id
      t.integer :number, default: 0, null: false

      t.timestamps
    end
  end
end
