class AddUserIdToShareMoneyAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :share_money_accounts, :user_id, :integer
  end
end
