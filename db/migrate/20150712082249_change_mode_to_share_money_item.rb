class ChangeModeToShareMoneyItem < ActiveRecord::Migration[5.1]
  def change
  	remove_column :share_money_items, :mode
    add_column :share_money_items, :mode, :string
  end
end
