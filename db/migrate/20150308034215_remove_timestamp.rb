class RemoveTimestamp < ActiveRecord::Migration[5.1]
  def change
  	remove_column :weathertmps, :timestamp
  end
end
