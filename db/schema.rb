# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170926124722) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "lunch_commodities", force: :cascade do |t|
    t.integer "store_id"
    t.string "name"
    t.integer "price"
    t.integer "count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lunch_forms", force: :cascade do |t|
    t.integer "store_id"
    t.integer "status"
    t.integer "sum_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lunch_orders", force: :cascade do |t|
    t.integer "form_id"
    t.integer "commodity_id"
    t.integer "count"
    t.string "owner"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lunch_stores", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.integer "count"
    t.datetime "last_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_book_books", force: :cascade do |t|
    t.integer "owner_id"
    t.string "name"
    t.string "currency_name"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_book_exchanges", force: :cascade do |t|
    t.integer "book_id"
    t.string "currency"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_book_flows", force: :cascade do |t|
    t.integer "item_id"
    t.integer "member_id"
    t.float "number", default: 0.0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_book_items", force: :cascade do |t|
    t.integer "book_id"
    t.string "title"
    t.float "price", default: 0.0, null: false
    t.datetime "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "exchange_id"
  end

  create_table "share_book_members", force: :cascade do |t|
    t.integer "book_id"
    t.integer "user_id"
    t.string "nick_name"
    t.string "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_money_accounts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "owner_id"
  end

  create_table "share_money_currencies", force: :cascade do |t|
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_money_flows", force: :cascade do |t|
    t.integer "item_id"
    t.integer "relation_id"
    t.float "num"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_money_items", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "timestamp"
    t.integer "account_id"
    t.integer "currency_id"
    t.integer "tag_id"
    t.string "mode"
  end

  create_table "share_money_managers", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_money_tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_money_ua_relations", force: :cascade do |t|
    t.integer "account_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_money_users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "password_digest"
  end

  create_table "shortened_urls", force: :cascade do |t|
    t.integer "owner_id"
    t.string "owner_type", limit: 20
    t.string "url", null: false
    t.string "unique_key", limit: 10, null: false
    t.integer "use_count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "expires_at"
    t.string "category"
    t.index ["category"], name: "index_shortened_urls_on_category"
    t.index ["owner_id", "owner_type"], name: "index_shortened_urls_on_owner_id_and_owner_type"
    t.index ["unique_key"], name: "index_shortened_urls_on_unique_key", unique: true
    t.index ["url"], name: "index_shortened_urls_on_url"
  end

  create_table "tmpstrmsgs", force: :cascade do |t|
    t.string "title"
    t.string "msg"
    t.string "more_msg"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "weathertmps", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "key"
    t.text "data"
  end

end
