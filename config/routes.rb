Rails.application.routes.draw do

  root 'welcome#index'
  get 'm_weather' => 'm_weather#index'
  post 'url_reduce_url' => 'url#reduce_url'
  match 'url' => 'url#index', via: [:get, :post]
  # get 'u/:id' => "shortener/shortened_urls#show"
  get 'u/:id' => "url#temp_redirect"
  
  get 'lunch' => 'lunch#index'
  get 'lunch/list' => 'lunch#list'
  get 'lunch/newstore/:name' => 'lunch#newstore'
  get 'lunch/newform/:name' => 'lunch#newform'
  post 'lunch/updateform' => 'lunch#updateform'
  get 'lunch/form/:id' => 'lunch#form'
  post 'lunch/updatecommodity' => 'lunch#updatecommodity'
  get 'lunch/manage' => 'lunch#manage'
  get 'lunch/manage_store' => 'lunch#manage_store'
  get 'lunch/remove' => 'lunch#remove'
  get 'lunch/remove_store' => 'lunch#remove_store'
  get 'lunch/:id' => 'lunch#show'

  # get 'share_money/newuser' => 'share_money_account#newuser'
  get 'share_money/signup' => 'share_money_account#signup'
  post 'share_money/createuser' => 'share_money_account#createuser'
  get 'share_money/newaccount' => 'share_money_account#newaccount'
  post 'share_money/createaccount' => 'share_money_account#createaccount'
  post 'share_money/createrel/:rid' => 'share_money_account#createrel'
  get 'share_money/account/:rid' => 'share_money_account#account'
  post 'share_money/createitem/:rid' => 'share_money_account#createitem'
  delete 'share_money/destroyitem/:id' => 'share_money_account#destroyitem'
  delete 'share_money/destroyitem' => 'share_money_account#destroyitem'
  get 'share_money' => 'share_money_account#index'
  get 'share_money/login' => 'share_money_account#login'
  post 'share_money/login' => 'share_money_account#dologin'
  get 'share_money/logout' => 'share_money_account#logout'
  # get 'share_money/:uid' => 'share_money_account#index'

  get 'tmpstrmsg' => 'tmpstrmsg#index'
  match 'tmpstrmsg/add' => 'tmpstrmsg#add', via: [:get, :post]

  namespace :share_book do
    get '' => 'home#index'
    post 'login' => 'home#login'
    post 'reset' => 'home#reset'
    get 'create_book' => 'home#create_book'
    resources :book, only: [:show, :update] do
      member do
        get 'members', 'logout'
        post 'create_item', 'create_member', 'create_exchange'
        patch 'update_item', 'update_member', 'update_exchange'
        delete 'delete_item', 'delete_exchange'
      end
    end
  end

  get 'test/get' => 'test_api#get'
  post 'test/post' => 'test_api#post'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
